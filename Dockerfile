if [ "$1" = "" ] ;
then
    echo "Enter the build number (example : SCA-2011-06-24-2.0) \c"
    read buildno
else
    buildno=$1
fi

if [ "$2" = "" ] ;
then
    echo "Select the server you want to deploy

    1 dev1

	"
    read server
else
    server=$2
fi



if [ "$server" = "1" ] ;
then
    folderName="dev1"
    cliport="9990"
    statusEnv="TEST"
    bi_name=dev1
    jbossenv="/home/vagrant/jboss-eap-7.0/bin"
    jboss_service_name="jboss-stg1.sh"

#  Added idsURL for indesign server
fi

echo "Editing the properties files  ..."

cd /home/vagrant/${folderName}/deploy/${buildno}/config

sed "s/<status>/$statusEnv/g" my.properties > my.properties.tmp && mv my.properties.tmp my.properties


echo "Editing the properties files (my.properties) ..."

cd /home/vagrant/${folderName}/deploy/${buildno}/config

echo "Copying files to ${folderName}/config folder ..."

cp /home/vagrant/${folderName}/deploy/${buildno}/config/log.xml /home/vagrant/${folderName}/config






echo "Copying of files to ${folderName}/config folder is successfully completed..."

############################## Modify the Deploy.txt ########################

echo "Editing the ${folderName}.cli file ..."

cd /home/vagrant/deploy
cp ${folderName}.cli ${folderName}.cli.bak
sed "s/cliport/${cliport}/g" ${folderName}.cli > ${folderName}.cli.tmp && mv ${folderName}.cli.tmp ${folderName}.cli
sed "s/folderName/${folderName}/g" ${folderName}.cli > ${folderName}.cli.tmp && mv ${folderName}.cli.tmp ${folderName}.cli
sed "s/buildno/${buildno}/g" ${folderName}.cli > ${folderName}.cli.tmp && mv ${folderName}.cli.tmp ${folderName}.cli

echo "Editing of ${folderName}.cli file done successfully..."


############################### Execute setupCmdLine.sh ########################

echo "Start CONNECTING TO command line interface for deployment..."

cd $jbossenv
export PATH=$jbossenv:$PATH
pwd

#echo "Stopping Obsidian app"
#/home/vagrant/deploy/stop-obs.sh $folderName

echo "Start executing the cli script to undeploy and deploy new app"

sh jboss-cli.sh --connect --controller=127.0.0.1:${cliport}  --file=/home/vagrant/deploy/${ }.cli




echo "deployed the app..."


cd /home/vagrant/deploy

cp ${folderName}.cli.bak ${folderName}.cli

echo "Restart JBoss service in order to avoid OutOfMemory issue during next re-deploy"
sudo service $jboss_service_name restart

echo "Sleeping for 60s in order to have JBoss up and running"
sleep 60s
echo "Starting Obsidian app"
#/home/vagrant/deploy/start-obs.sh $folderName


echo "Deployment of SCA completed..."

